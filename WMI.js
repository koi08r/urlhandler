/**
    (c) burchenyavp@murmangas.ru 04.03.2016
 */

var print = function(s){ return WScript.echo(s) } ;
var exit = function(e){ WScript.Quit(e) } ;



var Net     = new ActiveXObject("WScript.Network") ;
var FS      = new ActiveXObject("Scripting.FileSystemObject") ;
var Shell   = new ActiveXObject("WScript.Shell") ;


//var WMI = new ActiveXObject("WbemScripting.SWbemLocator").ConnectServer(".", "/root/cimv2") ;
var WMI = GetObject("WinMgmts://./root/cimv2");

//var pcItems = GetObject("winmgmts:\\.\root\cimv2").ExecQuery("SELECT * FROM Win32_ComputerSystem") ;
//var osItems = GetObject("winmgmts:\\.\root\cimv2").ExecQuery("SELECT * FROM Win32_OperatingSystem") ;
var pcItems = WMI.ExecQuery("SELECT * FROM Win32_ComputerSystem") ;
var osItems = WMI.ExecQuery("SELECT * FROM Win32_OperatingSystem") ;

for(var e = new Enumerator(pcItems) ; !e.atEnd() ; e.moveNext()){
    print(e.item().Name) ;
	print(e.item().UserName) ;
}
