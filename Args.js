/**
    (c) burchenyavp@murmangas.ru 04.03.2016
 */

var print = function(s){ return WScript.echo(s) } ;
var exit = function(e){ WScript.Quit(e) } ;
var argc = WScript.Arguments.length ;
var argv = (function(argv){
    var result = [] ;
    for(i=0 ; i<argv.length ; i++) {
        result.push(argv.item(i)) ;
    }
    return result ;    
})(WScript.Arguments) ;

var parse = function(args){
    var args = {} ;

    for(i=0 ; i<argc ; i++){
		var hasNext = (argc > i+1) ;
        var p = argv[i] ;
        var v = argv[i+1] ;
        var needValue = false ;

        switch(p){
            case '-bin' :
                args.bin = v ;
                needValue = true ;
            break ;
            case '-url' :
                args.url = v ;
                needValue = true ;
            break ;
            default:
                throw new Error('Unknown argument: ' + p) ;
        }

        if(needValue) {
            if(!v)
                throw new Error(p + ' need a value') ;
            i++ ;
        }
    }

    return args ;
}


var o = parse(argv) ;

print(
    'bin = ' + o.bin + "\n" +
    'url = ' + o.url + "\n"
) ;


