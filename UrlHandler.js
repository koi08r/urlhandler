/**
    JScript very fast and simple helper for url handle.
    (c) burchenyavp@murmangas.ru 03.03.2016

 */

var scriptName = WScript.ScriptName ;
var bin = "C:\\Program Files\\uvnc bvba\\UltraVNC\\vncviewer.exe" ;
var url = 'vnc://127.0.0.1?fullscreen=true' ;

var print = function(s){ return WScript.echo(s) } ;
var exit = function(e){ WScript.Quit(e) } ;
var argc = WScript.Arguments.length ;
var argv = (function(argv){
    var result = [] ;
    for(i=0 ; i<argv.length ; i++) {
        result.push(argv.item(i)) ;
    }
    return result ;    
})(WScript.Arguments) ;
var shell = new ActiveXObject("WScript.Shell") ;
var exec = function(bin, arguments){
    var cmd = "\"" + bin + "\"" ;
    for(i=0 ; i<arguments.length ; i++){
        cmd += ' ' + arguments[i] ;
	}


    print(cmd) ;
    shell.run(cmd, 0, true) ; 
} ;

if(argc==0){
    print(
        'Usage: '   + scriptName + " -bin \"<path to vncviewer.exe>\" -url \"<URL>\"" + "\n" +
        'Example: ' + scriptName + " -bin \"C:\\Program Files\\uvnc bvba\\UltraVNC\\vncviewer.exe\" -url \"vnc://127.0.0.1?fullscreen=true\"" + "\n"
    ) ;
    exit(255) ;
}


for(i=0 ; i<argc ; i++){

    if(argc <= i+1)
        throw new Error('Empty argument value for: ' + argv[i]) ;

    var p = argv[i] ;
    var v = argv[i+1] ;
    i++ ;

    switch(p){
        case '-bin' :
            bin = v ;
        break ;
        case '-url' :
            url = v ;
        break ;
        default:
            throw new Error('Unknown argument: ' + p) ;
	}
}

function Parser(url){

    if(!url || !url.length>0)
        throw new Error('Illegal url length') ;
    else
        this.url ;

    this.parse = function(callback){

        if(!url || !url.length>0 || typeof callback != 'function')
            throw new Error('Illegal callback function') ;

        // TODO: Protocol group (vnc,ssh)
        var match = /^vnc:\/\/([^\?\/]+)\/?(?:\?(.*))?$/gi.exec(url) ;
        if(match && match.length > 1){
		    var result = {} ;
            var host ;
            var port;
            var paramMap ;

            var hostPort = match[1].split(':') ;
            result.host = hostPort[0] ;
            result.port = hostPort[1] ;

            result.params = [] ;
            if(match[2].length > 0){
                paramMap = match[2].split('&') ;
                for(var i=0 ; i<paramMap.length ; i++){
                    var entry = paramMap[i].split('=') ;
                    result.params.push({
                        name : entry[0],
                        value : entry[1]
                    }) ;
                }
            }

        callback(result) ;
        } else
            throw new Error('Illegal url syntax') ;
    }
}

new Parser(url).parse(function(o){
    var arguments = [ o.host ] ;
    for(i=0 ; i <o.params.length ; i++){
        switch(o.params[i].name){
            case 'fullscreen' :
                if(o.params[i].value === 'true')
                arguments.push('/fullscreen') ;
            break ;
            default:
                throw new Error("Unsupported parameter: '" + o.params[i].name + "'") ;
		}
	}

	exec(bin, arguments) ;
}) ;

/*
new Parser(url).parse(function(o){
    var cmd = "\"" + bin + "\"" + ' ' + o.host ;
    for(i=0 ; i <o.params.length ; i++){
        switch(o.params[i].name){
            case 'fullscreen' :
                if(o.params[i].value === 'true')
                cmd += ' /fullscreen'
            break ;
            default:
                throw new Error("Unsupported parameter: '" + o.params[i].name + "'") ;
		}
	}

    print(cmd) ;
    shell.run(cmd, 0, true) ;
	exec(cmd, 0, true) ;
}) ;
*/

/*
new Parser(url).parse(function(o){
    print(
        'Bin = ' + bin + "\n" +
        'Url = ' + url + "\n" +
        'Host = ' + o.host + "\n" +
        'Port = ' + o.port + "\n" +
        // TODO: join function ;)
        (function(p){
            var r = '' ;
            for(i=0 ; i<p.length ; i++){
                r += p[i].name + ' = ' + p[i].value + "\n" ;
            }
            return r ;
		})(o.params)
    ) ;
}) ;
*/